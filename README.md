# investment platform restful application

To run locally with spring boot :

mvn spring-boot:run
service will be available at localhost:8080
project using H2 embedded database

## register user
 
 ```
POST /user

body example
{
  "email": "user@mauil.com",
  "firstName": "name",
  "lastName": "lname"
}

```

## get registered users

 ```
GET /user
response
[
  {
    "id": 1,
    "email": "user@mauil.com",
    "firstName": "name",
    "lastName": "lname"
  }
]

 ```

## get user by email

 ```
GET /user?email=user@mauil.com

{"id":1,"email":"user@mauil.com","firstName":"name","lastName":"lname"}

 ```

## Enter loan

 ```
POST /loan
{
  "amount": 1000,
  "apr": 100,
  "originator": "loan Generator"
}
 ```

## All loans

 ```
GET /loan
[
  {
    "id": 3,
    "amount": 1000,
    "apr": 100,
    "created": "2020-01-13T10:55:52.818",
    "originator": "loan Generator",
    "coveredByInvestments": 0
  }
]
 ```

## create Investment
 ```
POST /invest
{
  "amount": 100,
  "loanId": 3,
  "userId": 1
}

where loanId is loan you wish to invest to
userId is investing user Id

 ```
