package wandoo.investment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import wandoo.loan.Loan;
import wandoo.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Investment {

    private @Id @GeneratedValue Long id;
    private @Positive @NotNull BigDecimal amount;
    private @ManyToOne Loan loan;
    private LocalDateTime created;
    private @ManyToOne User user;

}
