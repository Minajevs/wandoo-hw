package wandoo.investment;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class InvestmentModel {

    private Long id;
    private @Positive @NotNull BigDecimal amount;
    private @NotNull Long loanId;
    private LocalDateTime created;
    private @NotNull Long userId;

}
