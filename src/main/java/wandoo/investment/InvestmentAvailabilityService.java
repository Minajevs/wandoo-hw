package wandoo.investment;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import wandoo.loan.Loan;

import java.math.BigDecimal;
import java.util.List;

@Service
@AllArgsConstructor
public class InvestmentAvailabilityService {

    private InvestmentRepository investmentRepository;

    public BigDecimal amountInvested(Loan loan) {
        List<Investment> investments = investmentRepository.findByLoan(loan);
        return investments.stream().map(Investment::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }


    public void validateAvailabilityForInvestment(Investment investment) {
        BigDecimal invested = amountInvested(investment.getLoan());
        if (invested.add(investment.getAmount()).compareTo(investment.getLoan().getAmount()) > 0) {
            throw new InvestmentAvailabilityException("amount available for investment is "
                    + investment.getLoan().getAmount().subtract(invested));
        }
    }

    class InvestmentAvailabilityException extends RuntimeException {
        public InvestmentAvailabilityException(String s) {
            super(s);
        }
    }
}
