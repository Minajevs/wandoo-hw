package wandoo.investment;

import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.metadata.TypeFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import wandoo.EntryNotFoundException;
import wandoo.loan.Loan;
import wandoo.loan.LoanRepository;
import wandoo.user.User;
import wandoo.user.UserRepository;

import java.time.LocalDateTime;

@Component
public class InvestmentMapping implements OrikaMapperFactoryConfigurer {

    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void configure(MapperFactory orikaMapperFactory) {
        orikaMapperFactory.classMap(InvestmentModel.class, Investment.class)
                .customize(new InvestmentMapper())
                .byDefault()
                .register();
    }

    class InvestmentMapper implements Mapper<InvestmentModel, Investment> {

        @Override
        public void mapAtoB(InvestmentModel investmentModel, Investment investment, MappingContext mappingContext) {
            Loan loan = loanRepository.findById(investmentModel.getLoanId()).orElseThrow(() -> new EntryNotFoundException("Loan does not exist " + investmentModel.getLoanId()));
            investment.setLoan(loan);
            User user = userRepository.findById(investmentModel.getUserId()).orElseThrow(() -> new EntryNotFoundException("User does not exist " + investmentModel.getUserId()));
            investment.setUser(user);
            investment.setCreated(investmentModel.getCreated() == null ? LocalDateTime.now() : investmentModel.getCreated());
        }

        @Override
        public void mapBtoA(Investment investment, InvestmentModel investmentModel, MappingContext mappingContext) {
            investmentModel.setLoanId(investment.getLoan().getId());
            investmentModel.setUserId(investment.getUser().getId());
        }

        @Override
        public void setMapperFacade(MapperFacade mapperFacade) {

        }

        @Override
        public void setUsedMappers(Mapper<Object, Object>[] mappers) {

        }

        @Override
        public Boolean favorsExtension() {
            return null;
        }

        @Override
        public Type<InvestmentModel> getAType() {
            return TypeFactory.valueOf(InvestmentModel.class);
        }

        @Override
        public Type<Investment> getBType() {
            return TypeFactory.valueOf(Investment.class);
        }
    }

}
