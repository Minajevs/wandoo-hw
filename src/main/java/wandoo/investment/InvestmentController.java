package wandoo.investment;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wandoo.EntryNotFoundException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/investment")
@AllArgsConstructor
class InvestmentController {

    private final InvestmentRepository repository;
    private final MapperFacade mapperFacade;
    private final InvestmentAvailabilityService investmentAvailabilityService;

    @GetMapping
    ResponseEntity all() {
        List<Investment> investments = repository.findAll();
        return ResponseEntity.ok(investments);
    }

    @PostMapping
    ResponseEntity<?> invest(@Valid @RequestBody InvestmentModel investmentModel) {
        Investment investment = mapperFacade.map(investmentModel, Investment.class);
        investmentAvailabilityService.validateAvailabilityForInvestment(investment);
        repository.save(investment);
        return ResponseEntity.status(HttpStatus.CREATED).body(investment);
    }

    @GetMapping("/{id}")
    ResponseEntity<Investment> byId(@PathVariable Long id) {
        Investment investment = repository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(id));
        return ResponseEntity.ok(investment);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> delete(@PathVariable Long id) {
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
