package wandoo.investment;

import org.springframework.data.jpa.repository.JpaRepository;
import wandoo.loan.Loan;

import java.util.List;

public interface InvestmentRepository extends JpaRepository<Investment, Long> {

    List<Investment> findByLoan(Loan loan);

}
