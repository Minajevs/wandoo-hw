package wandoo.loan;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.metadata.Type;
import ma.glasnost.orika.metadata.TypeFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.stereotype.Component;
import wandoo.investment.InvestmentAvailabilityService;

import java.math.BigDecimal;

@Component
@AllArgsConstructor
public class LoanMapping implements OrikaMapperFactoryConfigurer {

    private final InvestmentAvailabilityService investmentAvailabilityService;

    @Override
    public void configure(MapperFactory orikaMapperFactory) {
        orikaMapperFactory.classMap(LoanModel.class, Loan.class)
                .customize(new LoanMapper())
                .byDefault()
                .register();
    }

    class LoanMapper implements Mapper<LoanModel, Loan> {

        @Override
        public void mapAtoB(LoanModel loanModel, Loan loan, MappingContext mappingContext) {
        }

        @Override
        public void mapBtoA(Loan loan, LoanModel loanModel, MappingContext mappingContext) {
            BigDecimal invested = investmentAvailabilityService.amountInvested(loan);
            loanModel.setCoveredByInvestments(invested.divide(loan.getAmount()));
        }

        @Override
        public void setMapperFacade(MapperFacade mapperFacade) {

        }

        @Override
        public void setUsedMappers(Mapper<Object, Object>[] mappers) {

        }

        @Override
        public Boolean favorsExtension() {
            return null;
        }

        @Override
        public Type<LoanModel> getAType() {
            return TypeFactory.valueOf(LoanModel.class);
        }

        @Override
        public Type<Loan> getBType() {
            return TypeFactory.valueOf(Loan.class);
        }
    }

}
