package wandoo.loan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Loan {

    private @Id @GeneratedValue Long id;
    private @Positive @NotNull BigDecimal amount;
    private @Positive @NotNull BigDecimal apr;
    private LocalDateTime created;
    private @NotBlank String originator;

}
