package wandoo.loan;

import lombok.AllArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wandoo.EntryNotFoundException;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/loan")
class LoanController {

    private final LoanRepository repository;
    private final MapperFacade mapperFacade;

    @GetMapping
    ResponseEntity all() {
        List<Loan> loans = repository.findAll();
        return ResponseEntity.ok(mapperFacade.mapAsList(loans, LoanModel.class));
    }

    @PostMapping
    ResponseEntity<?> loan(@Valid @RequestBody Loan newLoan) {
        newLoan.setCreated(LocalDateTime.now());
        repository.save(newLoan);
        return ResponseEntity.status(HttpStatus.CREATED).body(newLoan);
    }

    @GetMapping("/{id}")
    ResponseEntity<Loan> one(@PathVariable Long id) {

        Loan loan = repository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(id));

        return ResponseEntity.ok(loan);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> delete(@PathVariable Long id) {
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
