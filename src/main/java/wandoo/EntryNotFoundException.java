package wandoo;

public class EntryNotFoundException extends RuntimeException {

	public EntryNotFoundException(Long id) {
		super("Could not find consumption " + id);
	}
	public EntryNotFoundException(String email) {
		super("Could not find consumption by email " + email);
	}
}
