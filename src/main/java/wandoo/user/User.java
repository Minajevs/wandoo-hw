package wandoo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private @Id @GeneratedValue Long id;
    @Column(unique = true)
    private @Email String email;
    private @NotBlank String firstName;
    private @NotBlank String lastName;

}
