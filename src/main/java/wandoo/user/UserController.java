package wandoo.user;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wandoo.EntryNotFoundException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
class UserController {

    private final UserRepository repository;

    @GetMapping
    ResponseEntity all() {
        List<User> users = repository.findAll();
        return ResponseEntity.ok(users);
    }

    @GetMapping("/{id}")
    ResponseEntity<User> byId(@PathVariable Long id) {
        User user = repository.findById(id)
                .orElseThrow(() -> new EntryNotFoundException(id));
        return ResponseEntity.ok(user);
    }

    @GetMapping(params = {"email"})
    ResponseEntity byEmail(@RequestParam("email") String email) {
        List<User> users = repository.findByEmail(email);
        return ResponseEntity.ok(users);
    }

    @PostMapping
    ResponseEntity<?> register(@Valid @RequestBody User user) {
        repository.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }
}
