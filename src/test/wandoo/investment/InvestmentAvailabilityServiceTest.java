package wandoo.investment;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import wandoo.loan.Loan;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@ExtendWith(MockitoExtension.class)
class InvestmentAvailabilityServiceTest {

    @Mock
    private InvestmentRepository investmentRepository;
    @InjectMocks
    private InvestmentAvailabilityService investmentAvailabilityService;
    @Mock
    private Loan loan;
    @Mock
    private Investment inv1, inv2, inv3;

    @Test
    void amountInvestedZero() {
        when(investmentRepository.findByLoan(loan)).thenReturn(Arrays.asList());

        assertEquals(BigDecimal.valueOf(0), investmentAvailabilityService.amountInvested(loan));
    }

    @Test
    void amountInvested900() {
        when(investmentRepository.findByLoan(loan)).thenReturn(Arrays.asList(inv1, inv2, inv3));
        when(inv1.getAmount()).thenReturn(BigDecimal.valueOf(500));
        when(inv2.getAmount()).thenReturn(BigDecimal.valueOf(300));
        when(inv3.getAmount()).thenReturn(BigDecimal.valueOf(100));

        assertEquals(BigDecimal.valueOf(900), investmentAvailabilityService.amountInvested(loan));
    }

    @Test
    void validateAvailabilityForInvestmentTrue() {
        when(investmentRepository.findByLoan(loan)).thenReturn(Arrays.asList(inv2, inv3));
        when(loan.getAmount()).thenReturn(BigDecimal.valueOf(1000));
        when(inv1.getAmount()).thenReturn(BigDecimal.valueOf(500));
        when(inv1.getLoan()).thenReturn(loan);
        when(inv2.getAmount()).thenReturn(BigDecimal.valueOf(300));
        when(inv3.getAmount()).thenReturn(BigDecimal.valueOf(100));
        investmentAvailabilityService.validateAvailabilityForInvestment(inv1);
    }

    @Test
    void validateAvailabilityForInvestmentFalse() {
        when(investmentRepository.findByLoan(loan)).thenReturn(Arrays.asList(inv2, inv3));
        when(loan.getAmount()).thenReturn(BigDecimal.valueOf(500));
        when(inv1.getAmount()).thenReturn(BigDecimal.valueOf(400));
        when(inv1.getLoan()).thenReturn(loan);
        when(inv2.getAmount()).thenReturn(BigDecimal.valueOf(300));
        when(inv3.getAmount()).thenReturn(BigDecimal.valueOf(100));

        assertThrows(InvestmentAvailabilityService.InvestmentAvailabilityException.class,
                () -> investmentAvailabilityService.validateAvailabilityForInvestment(inv1));

}
    }